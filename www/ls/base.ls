container = ig.containers.base
mapElement = document.createElement \div
  ..id = \map
container.appendChild mapElement

map = L.map do
  * mapElement
  * minZoom: 7,
    maxZoom: 13,
    zoom: 7
    center: [49.78, 15.5]
    maxBounds: [[48.3,11.6], [51.3,19.1]]

baseLayer = L.tileLayer do
  * "https://samizdat.cz/tiles/ton_b1/{z}/{x}/{y}.png"
  * attribution: 'mapová data &copy; přispěvatelé <a target="_blank" href="http://osm.org">OpenStreetMap</a>, obrazový podkres <a target="_blank" href="http://stamen.com">Stamen</a>, <a target="_blank" href="https://samizdat.cz">Samizdat</a>'

labelLayer = L.tileLayer do
  * "https://samizdat.cz/tiles/ton_l1/{z}/{x}/{y}.png"

L.Icon.Default.imagePath = "https://samizdat.cz/tools/leaflet/images"

baseLayer.addTo map
labelLayer.addTo map

zarizeni =
  ['Aviatická 12, Praha', 50.106213, 14.271100, 'Přijímací středisko Praha - Ruzyně', '<p><strong>PřS Ruzyně</strong><br /><em>Zařízení se nachází v areálu Mezinárodního letiště Václava Havla (SRA prostor terminálu 2).</em><br />Aviatická 12, 160 08 Praha - Ruzyně</p>', 'http://www.suz.cz/zarizeni-suz/prs-ruzyne/']
  ['Havířská 514, Brno', 49.187042, 16.355527, 'PřS a ZZC Zastávka', '<p><strong>PřS Zastávka</strong><br />Havířská 514, 664 84 Brno - venkov</p>', 'http://www.suz.cz/zarizeni-suz/prs-zastavka/']
  ['Rudé armády 1000, Kostelec nad Orlicí', 50.118922, 16.228070, 'Pobytové středisko Kostelec nad Orlicí', '<p><strong>PoS Kostelec</strong><br />Tř. Rudé armády 1000, 517 41 Kostelec nad Orlicí</p>', 'http://www.suz.cz/zarizeni-suz/pos-kostelec/']
  ['Na Kopci 5, Havířov', 49.796568, 18.428615, 'Pobytové a integrační středisko Havířov', '<p><strong>PoIS Havířov</strong><br />Na Kopci 5, 735 64 Havířov - Dolní Suchá</p>', 'http://www.suz.cz/zarizeni-suz/pos-havirov/']
  ['Na Kopci 5, Havířov', 49.796568, 18.428615, 'Pobytové a integrační středisko Havířov', '<p><strong>PoIS Havířov</strong><br />Na Kopci 5, 735 64 Havířov - Dolní Suchá</p>', 'http://www.suz.cz/zarizeni-suz/ias-havirov/']
  ['Palackého 18, Jaroměř', 50.339076, 15.927798, 'Integrační azylové středisko Jaroměř', '<p><strong>IAS Jaroměř</strong><br />Palackého 18, 551 02 Jaroměř</p>', 'http://www.suz.cz/zarizeni-suz/ias-jaromer/']
  ['Tovačovského 3, Brno', 49.193019, 16.643054, 'Integrační azylové středisko Brno', '<p><strong>IAS Brno</strong><br />Tovačovského 3, 636 00 Brno - Židenice</p>', 'http://www.suz.cz/zarizeni-suz/ias-brno/']
  ['Husitská cesta 217/4, Ústí nad Labem', 50.657287, 13.987639, 'Integrační azylové středisko Předlice', '<p><strong>IAS Předlice</strong><br />Husitská cesta 217/4, 400 05 Ústí nad Labem</p>', 'http://www.suz.cz/zarizeni-suz/ias-predlice/']
  ['Jezová 1501, Bělá pod Bezdězem', 50.535451, 14.799594, 'Zařízení pro zajištění cizinců Bělá - Jezová', '<p><strong>ZZC Bělá - Jezová</strong><br />Jezová 1501, 294 21 Bělá pod Bezdězem</p>', 'http://www.suz.cz/zarizeni-suz/zzc-bela-jezova/']
  ['Vyšní Lhoty 234, Vyšní Lhoty', 49.637507, 18.461788, 'Zařízení pro zajištění cizinců Vyšní Lhoty', '<p><strong>ZZC Vyšní Lhoty</strong><br />Vyšní Lhoty 234, 739 51 Vyšní Lhoty</p>', 'http://www.suz.cz/zarizeni-suz/zzc-vysni-lhoty/']
  ['Drahonice u Lubence 41, Podbořany', 50.148312, 13.3377363, 'Zařízení pro zajištění cizinců Drahonice', '<p><strong>ZZC Drahonice</strong><br />Drahonice u Lubence 41, 441 01  Podbořany</p>', 'http://www.suz.cz/zarizeni-suz/zzc-drahonice/']

for [address, lat, lon, name, desc, link] in zarizeni
  L.marker [lat, lon]
    ..bindPopup "#{desc}<a href='#{link}' target='_blank'>#{link}</a>"
    ..addTo map
